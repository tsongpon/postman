package com.krungsri.postman.repository;

import com.krungsri.postman.domain.Employee;

import java.util.List;

public interface EmployeeRepository {

    List<Employee> listEmployee();

    Employee getEmployeeById(String id);

    Employee getEmployeeByEmail(String email);
}
