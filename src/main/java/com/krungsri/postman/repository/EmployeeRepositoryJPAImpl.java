package com.krungsri.postman.repository;

import com.krungsri.postman.domain.Employee;
import com.krungsri.postman.domain.User;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import org.jboss.logging.Logger;

import java.util.List;

@ApplicationScoped
public class EmployeeRepositoryJPAImpl implements EmployeeRepository {

    private static final Logger LOGGER = Logger.getLogger(EmployeeRepositoryJPAImpl.class);

    @Inject
    private EntityManager em;

    @Override
    public List<Employee> listEmployee() {
        TypedQuery<Employee> query = em.createQuery("SELECT emp FROM Employee emp ", Employee.class);
        return query.getResultList();
    }

    @Override
    public Employee getEmployeeById(String id) {
        return em.find(Employee.class, id);
    }

    @Override
    public Employee getEmployeeByEmail(String email) {
        return null;
    }
}
