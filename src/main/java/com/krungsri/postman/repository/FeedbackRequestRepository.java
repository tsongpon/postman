package com.krungsri.postman.repository;

import com.krungsri.postman.domain.FeedbackRequest;

public interface FeedbackRequestRepository {

    void saveFeedbackRequest(FeedbackRequest feedbackRequest);

    FeedbackRequest findByPeriodRequesterAndRequestee(String period, String requesterId, String requesteeId);
}
