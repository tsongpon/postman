package com.krungsri.postman.repository;

import com.krungsri.postman.api.v1.resource.AuthenticationResource;
import com.krungsri.postman.domain.User;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import org.jboss.logging.Logger;

@ApplicationScoped
public class UserRepositoryJPAImpl implements UserRepository {

    private static final Logger LOG = Logger.getLogger(UserRepositoryJPAImpl.class);

    @Inject
    private EntityManager em;

    @Override
    public User getUserByEmailAndPassword(String userName, String password) {
        TypedQuery<User> query = em.createQuery("SELECT usr FROM User usr WHERE usr.userName = :userName AND usr.password = :password", User.class);
        query.setParameter("userName", userName);
        query.setParameter("password", password);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            LOG.warn("User not found for userName : " + userName);
            return null;
        }
    }
}
