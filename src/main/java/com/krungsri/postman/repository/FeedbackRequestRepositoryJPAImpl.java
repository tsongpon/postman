package com.krungsri.postman.repository;

import com.krungsri.postman.domain.FeedbackRequest;
import com.krungsri.postman.domain.User;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.TypedQuery;
import org.jboss.logging.Logger;

import java.util.UUID;

@ApplicationScoped
public class FeedbackRequestRepositoryJPAImpl implements FeedbackRequestRepository {

    private static final Logger LOGGER = Logger.getLogger(FeedbackRequestRepositoryJPAImpl.class);

    @Inject
    private EntityManager em;

    @Override
    public void saveFeedbackRequest(FeedbackRequest feedbackRequest) {
        LOGGER.debug("Saving feedback request requester: " + feedbackRequest.getRequester() + ", requestee: " + feedbackRequest.getRequestee());
        if (feedbackRequest.getId() == null) {
            feedbackRequest.setId(UUID.randomUUID().toString());
        }
        em.persist(feedbackRequest);
    }

    @Override
    public FeedbackRequest findByPeriodRequesterAndRequestee(String period, String requesterId, String requesteeId) {
        TypedQuery<FeedbackRequest> query = em.createQuery("SELECT req FROM FeedbackRequest req WHERE req.period = :period " +
                "AND req.requester.id = :requesterId AND req.requestee.id = :requesteeId", FeedbackRequest.class);
        query.setParameter("period", period);
        query.setParameter("requesterId", requesterId);
        query.setParameter("requesteeId", requesteeId);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            LOGGER.warn("FeedbackRequest not found");
            return null;
        }
    }
}
