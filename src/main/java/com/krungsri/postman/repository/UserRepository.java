package com.krungsri.postman.repository;

import com.krungsri.postman.domain.User;

public interface UserRepository {
    User getUserByEmailAndPassword(String email, String password);
}
