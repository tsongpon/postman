package com.krungsri.postman.domain;

import jakarta.persistence.*;

import java.time.LocalDateTime;

@Entity
@Table(indexes = {@Index(name = "requester_idx", columnList = "requester_id"),
        @Index(name = "requestee_idx", columnList = "requestee_id")},
        uniqueConstraints = {@UniqueConstraint(columnNames = {"requester_id", "requestee_id", "period"})})
public class FeedbackRequest {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "requester_id", referencedColumnName = "id")
    private Employee requester;

    @ManyToOne
    @JoinColumn(name = "requestee_id", referencedColumnName = "id")
    private Employee requestee;

    private String period;

    private LocalDateTime createdTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Employee getRequester() {
        return requester;
    }

    public void setRequester(Employee requester) {
        this.requester = requester;
    }

    public Employee getRequestee() {
        return requestee;
    }

    public void setRequestee(Employee requestee) {
        this.requestee = requestee;
    }

    public LocalDateTime getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(LocalDateTime createdTime) {
        this.createdTime = createdTime;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
