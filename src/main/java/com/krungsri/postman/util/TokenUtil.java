package com.krungsri.postman.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.krungsri.postman.domain.User;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.jwt.Claims;

import java.time.Duration;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;

public class TokenUtil {

    private static final int TOKEN_AGE_IN_SECS = 43200;

    public static String generateToken(User user) {
        var currentTimeInSecs = currentTimeInSecs();
        return Jwt.issuer("https://krungsri.com/postman/issuer")
                .upn(user.getEmployee().getEmail())
                .groups(new HashSet<>(Arrays.asList("User", "Admin")))
                .claim("employeeId", user.getEmployee().getId())
                .claim("userEmail", user.getEmployee().getEmail())
                .claim("name", user.getEmployee().getName())
                .claim(Claims.iat, (long) currentTimeInSecs)
                .claim(Claims.exp, (long) (currentTimeInSecs + 43200))
                .sign();
    }

    private static int currentTimeInSecs() {
        long currentTimeMS = System.currentTimeMillis();
        return (int) (currentTimeMS / 1000);
    }
}
