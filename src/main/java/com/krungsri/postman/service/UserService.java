package com.krungsri.postman.service;

import com.krungsri.postman.domain.User;

import java.util.List;

public interface UserService {

    User findUser(String userName, String password);
}
