package com.krungsri.postman.service;

import com.krungsri.postman.domain.Employee;

import java.util.List;

public interface EmployeeService {

    Employee findByEmail(String email);

    List<Employee> listAll();

    Employee findById(String id);
}
