package com.krungsri.postman.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.krungsri.postman.exception.AuthException;
import com.krungsri.postman.util.TokenUtil;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.jboss.logging.Logger;

import java.time.Duration;
import java.util.Date;
import java.util.UUID;

@ApplicationScoped
public class AuthServiceImpl implements AuthService {

    private static Logger LOG = Logger.getLogger(AuthServiceImpl.class);
    private static final int TOKEN_AGE_IN_HOUR = 12;

    private UserService userService;

    @Inject
    public AuthServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public String authenticateUser(String userName, String password) {
        LOG.debug("authenticating user "+ userName);
        var user = userService.findUser(userName, password);
        if (user == null) {
            LOG.warn("user not found for user " + userName);
            throw new AuthException("Invalid username or password");
        }

        return TokenUtil.generateToken(user);
    }
}
