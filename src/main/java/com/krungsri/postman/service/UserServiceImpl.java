package com.krungsri.postman.service;

import com.krungsri.postman.domain.User;
import com.krungsri.postman.repository.UserRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

@ApplicationScoped
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Inject
    public UserServiceImpl(UserRepository userRepository)  {
        this.userRepository = userRepository;
    }

    @Override
    public User findUser(String userName, String password) {
        return this.userRepository.getUserByEmailAndPassword(userName, password);
    }
}
