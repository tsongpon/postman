package com.krungsri.postman.service;

public interface AuthService {

    String authenticateUser(String userName, String password);
}
