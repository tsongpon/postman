package com.krungsri.postman.service;

import com.krungsri.postman.domain.Employee;
import com.krungsri.postman.repository.EmployeeRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;

import java.util.List;

@ApplicationScoped
public class EmployeeServiceImpl implements EmployeeService {

    private EmployeeRepository employeeRepository;

    @Inject
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public Employee findByEmail(String email) {
        return this.employeeRepository.getEmployeeByEmail(email);
    }

    @Override
    public List<Employee> listAll() {
        return employeeRepository.listEmployee();
    }

    @Override
    public Employee findById(String id) {
        return employeeRepository.getEmployeeById(id);
    }
}
