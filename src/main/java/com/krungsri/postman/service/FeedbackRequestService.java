package com.krungsri.postman.service;

import com.krungsri.postman.domain.FeedbackRequest;

import java.util.List;

public interface FeedbackRequestService {

    void saveFeedbackRequests(List<FeedbackRequest> feedbackRequests);
}
