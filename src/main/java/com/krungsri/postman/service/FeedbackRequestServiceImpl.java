package com.krungsri.postman.service;

import com.krungsri.postman.domain.FeedbackRequest;
import com.krungsri.postman.exception.DuplicateFeedbackRequest;
import com.krungsri.postman.exception.EmployeeNotFound;
import com.krungsri.postman.repository.FeedbackRequestRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import org.jboss.logging.Logger;

import java.util.List;

@ApplicationScoped
public class FeedbackRequestServiceImpl implements FeedbackRequestService {

    private static final Logger LOGGER = Logger.getLogger(FeedbackRequestServiceImpl.class);

    private final FeedbackRequestRepository feedbackRequestRepository;
    private final EmployeeService employeeService;

    @Inject
    public FeedbackRequestServiceImpl(FeedbackRequestRepository feedbackRequestRepository, EmployeeService employeeService) {
        this.feedbackRequestRepository = feedbackRequestRepository;
        this.employeeService = employeeService;
    }

    @Override
    @Transactional
    public void saveFeedbackRequests(List<FeedbackRequest> feedbackRequests) {
        feedbackRequests.forEach(e -> {
            var request = feedbackRequestRepository.findByPeriodRequesterAndRequestee(
                    e.getPeriod(), e.getRequester().getId(), e.getRequestee().getId());
            if (request != null) {
                LOGGER.warn("Duplicate feedback request for period "+ e.getPeriod()
                        + " ,requester " + e.getRequester().getId() + " requestee " + e.getRequestee().getId());
                throw new DuplicateFeedbackRequest("Duplicate feedback request for period "+ e.getPeriod()
                        + " ,requestee " + e.getRequester().getId() + " requestee " + e.getRequestee().getId());
            }
        });
        feedbackRequests.forEach(feedbackRequestRepository::saveFeedbackRequest);
    }
}
