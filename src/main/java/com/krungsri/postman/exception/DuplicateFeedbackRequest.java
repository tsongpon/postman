package com.krungsri.postman.exception;

public class DuplicateFeedbackRequest extends RuntimeException {
    public DuplicateFeedbackRequest(String message) {
        super(message);
    }
}
