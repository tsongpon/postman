package com.krungsri.postman.api.v1.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

import java.time.LocalDateTime;
import java.util.List;

@RegisterForReflection
public record FeedbackRequestDTO(String period, String requesterId, List<String> requesteeIds, LocalDateTime requestedTime) {
}
