package com.krungsri.postman.api.v1.resource;

import com.krungsri.postman.api.v1.dto.LoginRequest;
import com.krungsri.postman.api.v1.dto.LoginResponse;
import com.krungsri.postman.exception.AuthException;
import com.krungsri.postman.service.AuthService;
import com.krungsri.postman.api.v1.dto.ErrorResponse;
import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jboss.logging.Logger;

@Path("/v1")
public class AuthenticationResource {

    private static final Logger LOG = Logger.getLogger(AuthenticationResource.class);
    private final AuthService authService;

    @Inject
    public AuthenticationResource(AuthService authService) {
        this.authService = authService;
    }

    @POST
    @Path("/login")
    @PermitAll
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(LoginRequest loginRequest) {
        LOG.debug("user log in with username " + loginRequest.userName());
        try {
            var accessToken = authService.authenticateUser(loginRequest.userName(), loginRequest.password());
            var loginResponse = new LoginResponse(accessToken);
            return Response.status(Response.Status.OK).entity(loginResponse).build();
        } catch (AuthException e) {
            return Response.status(Response.Status.UNAUTHORIZED).entity(new ErrorResponse(e.getMessage())).build();
        }
    }
}
