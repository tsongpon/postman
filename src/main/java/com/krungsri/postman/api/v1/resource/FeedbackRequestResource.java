package com.krungsri.postman.api.v1.resource;

import com.krungsri.postman.api.v1.dto.ErrorResponse;
import com.krungsri.postman.api.v1.dto.FeedbackRequestDTO;
import com.krungsri.postman.domain.Employee;
import com.krungsri.postman.domain.FeedbackRequest;
import com.krungsri.postman.exception.DuplicateFeedbackRequest;
import com.krungsri.postman.exception.EmployeeNotFound;
import com.krungsri.postman.service.EmployeeService;
import com.krungsri.postman.service.FeedbackRequestService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.jboss.logging.Logger;

import java.util.ArrayList;

@Path("/v1")
public class FeedbackRequestResource {

    private static final Logger LOGGER = Logger.getLogger(FeedbackRequestResource.class);

    private final FeedbackRequestService feedbackRequestService;
    private final EmployeeService employeeService;

    @Inject
    public FeedbackRequestResource(FeedbackRequestService feedbackRequestService, EmployeeService employeeService) {
        this.feedbackRequestService = feedbackRequestService;
        this.employeeService = employeeService;
    }

    @Path("/feedbackrequests")
    @POST
    @RolesAllowed({"Admin", "User"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFeedbackRequest(FeedbackRequestDTO feedbackRequestDTO) {
        var feedbackRequests = new ArrayList<FeedbackRequest>();
        var requester = employeeService.findById(feedbackRequestDTO.requesterId());
        if (requester == null) {
            LOGGER.warn("Requester id " + feedbackRequestDTO.requesterId() + " is not found");
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Requester id "
                    + feedbackRequestDTO.requesterId() + " is not found")).build();
        }
        for (String e : feedbackRequestDTO.requesteeIds()) {
            var feedbackRequest = new FeedbackRequest();
            feedbackRequest.setRequester(requester);
            var requestee = employeeService.findById(e);
            if (requestee == null) {
                LOGGER.warn("Requester id " + feedbackRequestDTO.requesterId() + " is not found");
                return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse("Requestee id "
                        + e + " is not found")).build();
            }
            feedbackRequest.setRequestee(requestee);
            feedbackRequest.setPeriod(feedbackRequestDTO.period());
            feedbackRequest.setCreatedTime(feedbackRequestDTO.requestedTime());
            feedbackRequests.add(feedbackRequest);
        }
        try {
            feedbackRequestService.saveFeedbackRequests(feedbackRequests);
        } catch (DuplicateFeedbackRequest e) {
            LOGGER.warn("Exception while saving feedback request", e);
            return Response.status(Response.Status.BAD_REQUEST).entity(new ErrorResponse(e.getMessage())).build();
        } catch (Exception e) {
            LOGGER.error("Unexpected error", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
        return Response.status(Response.Status.CREATED).build();
    }
}
