package com.krungsri.postman.api.v1.resource;

import com.krungsri.postman.api.v1.dto.EmployeeResponse;
import com.krungsri.postman.api.v1.dto.QueryResponse;
import com.krungsri.postman.service.EmployeeService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.SecurityContext;
import org.jboss.logging.Logger;

@Path("/v1")
public class EmployeeResource {

    private static final Logger LOGGER = Logger.getLogger(EmployeeResource.class);

    private final EmployeeService employeeService;

    @Inject
    public EmployeeResource(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GET
    @Path("/employees")
    @RolesAllowed({"Admin", "User"})
    @Produces(MediaType.APPLICATION_JSON)
    public Response GetEmployee(@Context SecurityContext ctx) {
        var employees = this.employeeService.listAll();
        var response = new QueryResponse<EmployeeResponse>();
        response.setData(employees.stream().map(e -> new EmployeeResponse(e.getId(), e.getName(), e.getEmail())).toList());
        return Response.status(Response.Status.OK).entity(response).build();
    }
}
