package com.krungsri.postman.api.v1.dto;

public record EmployeeResponse(String id, String name, String email) {
}
