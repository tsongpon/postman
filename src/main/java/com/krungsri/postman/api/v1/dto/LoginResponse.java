package com.krungsri.postman.api.v1.dto;

import io.quarkus.runtime.annotations.RegisterForReflection;

@RegisterForReflection
public record LoginResponse(String token){}
