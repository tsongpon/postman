package com.krungsri.postman.api.v1.dto;

import java.util.List;

public class QueryResponse<E> {
    private List<E> data;

    public List<E> getData() {
        return data;
    }

    public void setData(List<E> data) {
        this.data = data;
    }
}
